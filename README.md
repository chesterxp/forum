# Strona Zespołu Froum

*Sprawdzamy optymalizacje*

**Network** 3G Regular 750/250/100

| Changes                               | DOM Load      | Full Load     |
| -------------                         |:------------  | -----:        |
| Start                                 | 20:13         | 71:00         |
| MinCSS i JS                           | 55:72         | 71:00         |
| Add Lazy Load                         | 19.38         | 26.25         |